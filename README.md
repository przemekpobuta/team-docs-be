# Team Docs Backend

Backend for Team Docs app, collaboration spece for teams.

## Getting started

To get the Node server running locally:

- Clone this repo
- `npm install` to install all required dependencies
- Install MongoDB Community Edition and run it by executing `mongod`
- `npm run build` to build
- `npm start` to start the local server

## Commands

| Npm Script    | Description                                                                               |
| ------------- | ----------------------------------------------------------------------------------------- |
| `start`       | Does the same as 'npm run serve'. Can be invoked with `npm start`                         |
| `build`       | Full build. Runs ALL build tasks (`build-ts`, `tslint`)                                   |
| `serve`       | Runs node on `dist/server.js` which is the apps entry point                               |
| `watch-node`  | Runs node with nodemon so the process restarts if it crashes. Used in the main watch task |
| `watch`       | Runs all watch tasks (TypeScript, Node). Use this if you're not touching static assets.   |
| `test`        | Runs tests using Jest test runner                                                         |
| `watch-test`  | Runs tests in watch mode                                                                  |
| `build-ts`    | Compiles all source `.ts` files to `.js` files in the `dist` folder                       |
| `watch-ts`    | Same as `build-ts` but continuously watches `.ts` files and re-compiles when needed       |
| `tslint`      | Runs TSLint on project files                                                              |
| `debug`       | Performs a full build and then serves the app in watch mode                               |
| `serve-debug` | Runs the app with the --inspect flag                                                      |
| `watch-debug` | The same as `watch` but includes the --inspect flag so you can attach a debugger          |

## Project Structure

| Name                | Description                                                                                                |
| ------------------- | ---------------------------------------------------------------------------------------------------------- |
| **.vscode**         | Contains VS Code specific settings                                                                         |
| **dist**            | Contains the distributable (or output) from your TypeScript build. This is the code you ship               |
| **node_modules**    | Contains all your npm dependencies                                                                         |
| **src**             | Contains your source code that will be compiled to the dist dir                                            |
| **src/config**      | Passport authentication strategies and login middleware. Add other complex config code here                |
| **src/controllers** | Controllers define functions that respond to various http requests                                         |
| **src/models**      | Models define Mongoose schemas that will be used in storing and retrieving data from MongoDB               |
| **src/types**       | Holds .d.ts files not found on DefinitelyTyped. Covered more in this [section](#type-definition-dts-files) |
| **src**/server.ts   | Entry point to your express app                                                                            |
| **test**            | Contains your tests. Seperate from source because there is a different build process.                      |
| **views**           | Views define how your app renders on the client. In this case we're using pug                              |
| .env.example        | API keys, tokens, passwords, database URI. Clone this, but don't check it in to public repos.              |
| jest.config.js      | Used to configure Jest                                                                                     |
| package.json        | File that contains npm dependencies as well as [build scripts](#what-if-a-library-isnt-on-definitelytyped) |
| tsconfig.json       | Config settings for compiling server code written in TypeScript                                            |
| tsconfig.tests.json | Config settings for compiling tests written in TypeScript                                                  |
| tslint.json         | Config settings for TSLint code style checking                                                             |
