import { Request, Response } from 'express';
import { User } from '../models/user';
import logger from '../utils/logger';

export class UserController {
  /**
   * POST /users
   * Register user.
   */
  async registerUser(req: Request, res: Response) {
    const user = new User({
      email: req.body.email,
      password: req.body.password,
      fullName: req.body.fullName
    });

    try {
      await user.save();
      const token = await user.generateAuthToken();
      res.status(201).send({ user, token });
    } catch (err) {
      logger.debug('registerUser error:', err);
      res.status(400).send(err);
    }
  }

  /**
   * POST /users/login
   * Login.
   */
  async loginUser(req: Request, res: Response) {
    logger.debug('loginUser req:', req.body);
    try {
      const user = await User.findByCredentials(
        req.body.email,
        req.body.password
      );
      const token = await user.generateAuthToken();
      res.send({ user, token });
    } catch (err) {
      logger.debug('loginUser error:', err);
      res.status(400).send();
    }
  }

  /**
   * POST /users/logout
   * Logout user, delete current token.
   */
  async logoutUser(req: any, res: Response) {
    try {
      req.user.tokens = req.user.tokens.filter((token: any) => {
        return token.token !== req.token;
      });
      await req.user.save();

      res.send();
    } catch (err) {
      logger.error('logoutUser error:', err);
      res.status(500).send();
    }
  }

  /**
   * POST /users/logoutAll
   * Logout user, delete all tokens.
   */
  async logoutAllUsers(req: any, res: Response) {
    try {
      req.user.tokens = [];
      await req.user.save();
      res.send();
    } catch (err) {
      logger.error('logoutAllUser', err);
      res.status(500).send();
    }
  }

  /**
   * GET /users/me
   * Account data
   */
  async getUserAccount(req: Request, res: Response) {
    res.send(req.user);
  }

  /**
   * POST /users/me
   * Update account data
   */
  async updateUserAccount(req: Request, res: Response) {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['name', 'email', 'password', 'age'];
    const isValidOperation = updates.every((update: string) =>
      allowedUpdates.includes(update)
    );

    if (!isValidOperation) {
      return res.status(400).send({ error: 'Invalid updates!' });
    }

    try {
      updates.forEach(update => (req.user[update] = req.body[update]));
      await req.user.save();
      res.send(req.user);
    } catch (e) {
      res.status(400).send(e);
    }
  }

  /**
   * DELETE /users/me
   * Delete own account.
   */
  async deleteUserAccount(req: Request, res: Response) {
    try {
      await req.user.remove();
      res.send(req.user);
    } catch (e) {
      res.status(500).send();
    }
  }
}
