import * as mongoose from 'mongoose';
import { Request, Response } from 'express';

import { DocSchema } from '../models/doc';

const Doc = mongoose.model('Doc', DocSchema);

export class DocController {
  /**
   * GET /doc
   * Get list of docs.
   */
  getDocs(req: Request, res: Response) {
    Doc.find({}, (err, doc) => {
      if (err) {
        res.send(err);
      }
      res.json(doc);
    }).populate('created_by');
  }

  /**
   * POST /doc
   * Add new doc.
   */
  addDoc(req: Request, res: Response) {
    const newDoc = new Doc({
      ...req.body,
      created_by: req.user
    });

    newDoc.save((err, doc) => {
      if (err) {
        res.send(err);
      }
      res.json(doc);
    });
  }

  /**
   * GET /doc/:docId
   * Get single doc.
   */
  getDocWithID(req: Request, res: Response) {
    Doc.findById(req.params.docId, (err, doc) => {
      if (err) {
        res.send(err);
      }
      if (doc) {
        res.json(doc);
      }
      res.status(404).send();
    }).populate('created_by');
  }

  /**
   * UPDATE /doc/:docId
   * Update single doc.
   */
  updateDoc(req: Request, res: Response) {
    const updatedDoc = {
      ...req.body,
      updated_date: Date.now()
    };

    Doc.findOneAndUpdate({ _id: req.params.docId }, updatedDoc, err => {
      if (err) {
        res.send(err);
      }
      res.json({ message: 'Successfully updated document!' });
    });
  }

  /**
   * DELETE /doc/:docId
   * Delete single doc.
   */
  deleteDoc(req: Request, res: Response) {
    Doc.remove({ _id: req.params.docId }, (err: any) => {
      if (err) {
        res.send(err);
      }
      res.json({ message: 'Successfully deleted document!' });
    });
  }
}
