import mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const DocSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  updated_date: {
    type: Date
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});
