import { Document, Schema, Model, model } from 'mongoose';
import * as bcrypt from 'bcryptjs';
import * as crypto from 'crypto';
import validator from 'validator';
import jwt from 'jsonwebtoken';

import { SESSION_SECRET } from '../config/env';
import { IUserDocument } from '../interfaces/user';

// Instance methods?
export interface IUser extends IUserDocument {
  comparePassword(password: string): boolean;
  generateAuthToken(): string;
  generateGravatar(): string;
  toJSON(): any;
}

// Static methods
export interface IUserModel extends Model<IUser> {
  findByCredentials(email: string, password: string): IUser;
}

export const userSchema = new Schema(
  {
    email: {
      type: String,
      unique: true,
      required: true,
      trim: true,
      lowercase: true,
      validate(value: string): any {
        if (!validator.isEmail(value)) {
          throw new Error('Email is invalid');
        }
      }
    },
    password: {
      type: String,
      required: true,
      minlength: 7,
      trim: true,
      validate(value: string): any {
        if (value.toLowerCase().includes('password')) {
          throw new Error('Password cannot contain "password"');
        }
      }
    },
    fullName: {
      type: String,
      required: false,
      minlength: 3
    },
    pictureUrl: {
      type: String,
      required: false
    },
    tokens: [
      {
        token: {
          type: String,
          required: true
        }
      }
    ]
  },
  { timestamps: true }
);

/**
 * Remove sensitive user data from response
 */
userSchema.method('toJSON', function(): any {
  const userObject = this.toObject();

  delete userObject.password;
  delete userObject.tokens;
  delete userObject.updatedAt;

  return userObject;
});

userSchema.method('generateAuthToken', async function() {
  const token = jwt.sign({ _id: this._id.toString() }, SESSION_SECRET);

  this.tokens = this.tokens.concat({ token });
  await this.save();

  return token;
});

userSchema.static('findByCredentials', async function(
  email: string,
  password: string
) {
  const user = await User.findOne({ email });

  if (!user) {
    throw new Error('Unable to login');
  }

  const isMatch = await bcrypt.compare(password, user.password);

  if (!isMatch) {
    throw new Error('Unable to login');
  }

  return user;
});

/**
 * Pre hook before save
 */
userSchema.pre<IUser>('save', async function(next) {
  // Hash the plain text password before saving
  if (this.isModified('password')) {
    this.password = await bcrypt.hash(this.password, 8);
  }

  // Generate avatar URL
  this.pictureUrl = this.generateGravatar();

  next();
});

userSchema.virtual('docs', {
  ref: 'Doc',
  localField: '_id',
  foreignField: 'created_by'
});

// TODO: Add avatar to user
/**
 * Helper method for getting user's gravatar.
 */
userSchema.methods.generateGravatar = function(size?: number) {
  if (!size) {
    size = 200;
  }
  if (!this.email) {
    return `https://gravatar.com/avatar/?s=${size}&d=mp`;
  }
  const md5 = crypto
    .createHash('md5')
    .update(this.email)
    .digest('hex');
  return `https://gravatar.com/avatar/${md5}?s=${size}&d=mp`;
};

export const User: IUserModel = model<IUser, IUserModel>('User', userSchema);

export default User;
