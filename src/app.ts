import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import compression from 'compression';
import expressValidator from 'express-validator';

import { MONGODB_URI } from './config/env';
import { DocRoutes } from './routes/doc';
import { UserRoutes } from './routes/user';

class App {
  public app: express.Application;

  public docRoutes: DocRoutes = new DocRoutes();
  public userRoutes: UserRoutes = new UserRoutes();

  constructor() {
    this.app = express();
    this.config();
    this.initializeRoutes();
    this.mongoSetup();
  }

  private config(): void {
    // Express configuration
    this.app.set('port', process.env.PORT || 3000);
    this.app.use(compression());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(expressValidator());
    this.app.use(cors());
  }

  private initializeRoutes(): void {
    this.app.use('/api/auth/', this.userRoutes.router);
    this.app.use('/api/doc/', this.docRoutes.router);
  }

  // Connect to MongoDB
  private mongoSetup(): void {
    const mongoUrl = MONGODB_URI;
    (<any>mongoose).Promise = global.Promise;
    mongoose.set('debug', true);
    mongoose.connect(mongoUrl, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true
    });
  }
}

export default new App().app;
