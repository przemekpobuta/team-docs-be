import * as jwt from 'jsonwebtoken';
import User, { IUserModel } from '../models/user';
import { Request, Response, NextFunction } from 'express';
import logger from '../utils/logger';
import { SESSION_SECRET } from '../config/env';

export const authMiddleware = async (
  req: any,
  res: Response,
  next: NextFunction
) => {
  try {
    const reqAuthHeader = req.header('Authorization');
    if (!reqAuthHeader) {
      throw new Error('No reqAuthHeader');
    }
    const token = reqAuthHeader.replace('Bearer ', '');
    const decoded: any = jwt.verify(token, SESSION_SECRET);
    const user = await User.findOne({
      _id: decoded._id,
      'tokens.token': token
    });

    if (!user) {
      throw new Error('No user');
    }

    req.token = token;
    req.user = user;

    next();
  } catch (err) {
    logger.error('authMiddleware', err);
    res.status(401).send({ error: 'Please authenticate.' });
  }
};
