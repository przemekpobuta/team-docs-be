import { Document } from 'mongoose';

export interface IUserDocument extends Document {
  email: string;
  password: string;
  fullName: string;
  pictureUrl: string;
  tokens: string[];
}
