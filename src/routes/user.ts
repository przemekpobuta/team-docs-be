import { Application, Router } from 'express';

import { UserController } from '../controllers/user';
import { authMiddleware } from '../middleware/auth';

export class UserRoutes {
  public userController: UserController = new UserController();
  public router = Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post('/register', this.userController.registerUser);
    this.router.post('/login', this.userController.loginUser);
    this.router.post('/logout', authMiddleware, this.userController.logoutUser);
    this.router.post(
      '/logoutAll',
      authMiddleware,
      this.userController.logoutAllUsers
    );
    this.router.get('/me', authMiddleware, this.userController.getUserAccount);
    this.router.post(
      '/me',
      authMiddleware,
      this.userController.updateUserAccount
    );
    this.router.delete(
      '/me',
      authMiddleware,
      this.userController.deleteUserAccount
    );
  }
}
