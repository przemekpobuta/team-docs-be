import { DocController } from '../controllers/doc';
import { Application, Router } from 'express';
import { authMiddleware } from '../middleware/auth';

export class DocRoutes {
  public docController: DocController = new DocController();

  public router = Router();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get('/', authMiddleware, this.docController.getDocs);
    this.router.post('/', authMiddleware, this.docController.addDoc);
    this.router.get('/:docId', authMiddleware, this.docController.getDocWithID);
    this.router.put('/:docId', authMiddleware, this.docController.updateDoc);
    this.router.delete('/:docId', authMiddleware, this.docController.deleteDoc);
  }
}
